import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner numbers = new Scanner(System.in);
        System.out.print("Enter first number:");
        int a = numbers.nextInt();
        System.out.print("Enter second number:");
        int b = numbers.nextInt();
        if (b < 1) {
            System.out.print("Enter second number greater than zero:");
            b = numbers.nextInt();
        }

        try {
            float result = doDivision(a, b);
            System.out.println("Result is: " + result);
        } catch (ArithmeticException e) {
            System.out.println("Cannot divide by zero try again!");
            System.out.print("Enter first number:");
            a = numbers.nextInt();
            System.out.print("Enter second number:");
            b = numbers.nextInt();

            try {
                float result = doDivision(a, b);
                System.out.println("Result is: " + result);
            } catch (ArithmeticException e2) {
                System.out.println("Cannot divide by zero.");
            }
        } finally {
            System.out.println("Program close");
        }

    }

    public static float doDivision(int num1, int num2) throws ArithmeticException {
        float res = (num1 / num2);
        return res;
    }
}
